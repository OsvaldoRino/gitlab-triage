require 'spec_helper'

require 'gitlab/triage/filters/merge_request_date_conditions_filter'

describe Gitlab::Triage::Filters::MergeRequestDateConditionsFilter do
  let(:created_at) { Date.new(2016, 1, 31) }
  let(:updated_at) { Date.new(2017, 1, 1) }
  let(:merged_at) { Date.new(2016, 12, 31) }

  let(:resource) do
    {
      created_at: created_at,
      updated_at: updated_at,
      merged_at: merged_at
    }
  end
  let(:condition) do
    {
      attribute: 'merged_at',
      condition: 'older_than',
      interval_type: 'months',
      interval: 3
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a filter'
  it_behaves_like 'an issuable date filter'

  describe '#resource_value' do
    it 'has the correct value for the merged_at attribute' do
      filter = described_class.new(resource, condition)
      expect(filter.resource_value).to eq(merged_at)
    end
  end

  describe '#calculate ' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to eq(true)
    end
  end

  context 'when merged_at attribute is nil' do
    let(:resource) do
      {
        created_at: created_at,
        updated_at: updated_at,
        merged_at: nil
      }
    end

    describe '#calculate' do
      it 'calculates false given correct condition' do
        expect(subject.calculate).to eq(false)
      end
    end
  end
end
